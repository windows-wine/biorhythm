; � Drugwash 2012-2023
; Physical=23 Emotional=28 Intellectual=33 days
; Spiritual=53 Awareness=48 Aesthetic=43 Intuition=38 days

#NoEnv
#SingleInstance, Force
ListLines, Off
SetTitleMatchMode, Slow
SetControlDelay, -1
SetWinDelay, -1
SetBatchLines, -1
SetFormat, FloatFast, 0.15
SetFormat, Integer, D
StringCaseSense, Off
DetectHiddenWindows, On
Menu, Tray, UseErrorLevel
Menu, Main, UseErrorLevel
;DebugBIF()
;================================================================
;@Ahk2Exe-Obey bits, = %A_PtrSize% * 8
;@Ahk2Exe-Obey xbit, = "%A_PtrSize%" > 4 ? "x64" : "x86"
;@Ahk2Exe-Obey type, = "%A_IsUnicode%" ? "Unicode" : "ANSI"
;@Ahk2Exe-Obey xtype, = "%A_IsUnicode%" ? "W" : "A"
;@Ahk2Exe-Let version = 1.0.2.1
;@Ahk2Exe-AddResource *14 %A_ScriptDir%\res\Biorhythm.ico, 159
;@Ahk2Exe-SetFileVersion %U_version%
;@Ahk2Exe-SetProductVersion 1.0.0.0
;@Ahk2Exe-SetProductName Biorhythm
;@Ahk2Exe-SetInternalName Biorhythm.ahk
;@Ahk2Exe-SetOrigFilename Biorhythm.exe
;@Ahk2Exe-SetDescription Biorhythm displays biorhythmic chart for the near time period
;@Ahk2Exe-SetCompanyName Drugwash Hobby Programming
;@Ahk2Exe-SetCopyright Copyright � Drugwash`, August 2023
;@Ahk2Exe-SetLegalTrademarks Released under the terms of the GNU General Public License
;@Ahk2Exe-Set Comments, Compiled in %A_AhkVersion% %U_type% %U_bits%bit.
;@Ahk2Exe-SetMainIcon %A_ScriptDir%\res\Biorhythm.ico
;@Ahk2Exe-UpdateManifest 0, Drugwash.HobbyProgramming.Biorhythm, %U_version%
;========================================================
appname=Biorhythm
version=1.0.2.1
releaseD=August 16, 2023
releaseT = public							; release type (internal / public)
iconlocal = %A_ScriptDir%\res\%appname%.ico	; icon for uncompiled script
;========================================================
#include lib\updates.ahk
pToken := Gdip_Startup()
IfNotExist, %A_ScriptDir%\res\rmchart.dll
	{
	FileCreateDir, %A_ScriptDir%\res
	FileInstall, res\rmchart.dll, %A_ScriptDir%\res\rmchart.dll
	FileInstall, res\RMChart.chm, %A_ScriptDir%\res\RMChart.chm
	FileInstall, res\RMChart_license.txt, %A_ScriptDir%\res\RMChart_license.txt
	if !pToken
		{
		GetSysPath(sysfld)
		FileInstall, res\gdiplus.dll, %sysfld%\gdiplus.dll
		pToken := Gdip_Startup()
		}
	}
FileInstall, res\Biorhythm_help.txt, %A_ScriptDir%\res\%appname%_help.txt, 1
mylink := "drugwash" Chr(64) "mail" Chr(46) "com"
ahklink := "http://www" Chr(46) "autohotkey" Chr(46) "com"
inifile=%A_ScriptDir%\%appname%_preferences.ini
rmlib := A_ScriptDir "\res\rmchart.dll"
if !hRMC := DllCall("LoadLibrary", "Str", rmlib)
	{
	MsgBox, 0x43010, %appname%, Cannot load RMChart library!`nApplication will now exit.
	ExitApp
	}
legend := "Physical*Emotional*Intellectual*Intuition*Aesthetic*Awareness*Spiritual"
colors := "0xFF00FFFF,0xFFFF00FF,0xFF00FF00,0xFFFFFF00,0xFFFFC0CB,0xFF6633FF,0xFFFF6347"
span=31		; days to show
PI := 4*ATan(1)
cycle1=23	; Physical
cycle2=28	; Emotional
cycle3=33	; Intellectual
cycle4=38	; Intuition
cycle5=43	; Aesthetic
cycle6=48	; Awareness
cycle7=53	; Spiritual
DT=19660719
FormatTime, date, %DT%, dddd`, dd MMMM yyyy
name := "<author>"
profiles=
gosub makespan
IniRead, prCount, %inifile%, Generic, ProfileCount, 0
idx=0
Loop
	{
	if (idx=prCount)
		break
	IniRead, n%A_Index%, %inifile%, %A_Index%, Name, %A_Space%
	IniRead, d%A_Index%, %inifile%, %A_Index%, Date, %A_Space%
	if !n%A_Index%
		continue
	profiles .= n%A_Index% "|"
	idx++
	}
if !profiles
	profiles := name "|"
StringTrimRight, profiles, profiles, 1
IniRead, lastDate, %inifile%, Generic, LastDate, %DT%
IniRead, lastN, %inifile%, Generic, LastName, 1
IniRead, recentcount, %inifile%, Generic, Recent, 5
IniRead, AOT, %inifile%, Generic, AlwaysOnTop, 0
IniRead, fullset, %inifile%, Generic, FullSet, 0
IniRead, pastfuture, %inifile%, Generic, PastAndFuture, 1
IfNotExist, %inifile%
	{
	IniWrite, %recentcount%, %inifile%, Generic, Recent
	IniWrite, %AOT%, %inifile%, Generic, AlwaysOnTop
	IniWrite, %fullset%, %inifile%, Generic, FullSet
	IniWrite, %pastfuture%, %inifile%, Generic, PastAndFuture
	IniWrite, %A_Space%, %inifile%, Recent, 1
	}
if lastN
	{
	name := n%lastN%
	DT := lastDate
	}
hCursH := DllCall("LoadCursor", Ptr, NULL, "Int", 32649, Ptr)	; IDC_HAND
;========================================================
;	CUSTOM TRAY MENU
;========================================================
Menu, Tray, Tip, %appname% %version%
Menu, Tray, Icon, % A_IsCompiled ? "" : iconlocal
Menu, Tray, NoStandard
Menu, Tray, Add, Settings, options
Menu, Tray, Default, Settings
Menu, Tray, Add, Always on top, AOT
Menu, Tray, Add
Menu, Tray, Add, Details, help
Menu, Tray, Add, About, about
Menu, Tray, Add
Menu, Tray, Add, Reload, reload
Menu, Tray, Add, Exit, getout
Menu, Tray, Color, White
if AOT
	Menu, Tray, Check, Always on top
;========================================================
;	MAIN MENU
;========================================================
Menu,MenuLevel5,Add, &Details, help
Menu,MenuLevel5,Add, &RMChart Help, helpRMC
Menu,MenuLevel5,Add, RMChart &Licence, licence
Menu,MenuLevel5,Add
Menu,MenuLevel5,Add, &About, about
Menu,MenuLevel4,Add, &Settings, options
Menu,MenuLevel4,Add
Menu,MenuLevel4,Add, &Always on top, AOT
Menu,MenuLevel4,Add, &Full cycle set, fullset
Menu,MenuLevel4,Add, &Past && future, pastfuture
Menu,MenuLevel3,Add, &Clear list
Menu,MenuLevel3,Add
Menu,MenuLevel2,Add, &New profile, new
Menu,MenuLevel2,Add
Menu,MenuLevel2,Add, &Load profile, load
Menu,MenuLevel2,Add, &Edit profile, edit
Menu,MenuLevel2,Add, &Delete profile, delete
Menu,MenuLevel2,Add
Menu,MenuLevel2,Add, E&xit, GuiClose
Menu,MyMenu,Add, &File, :MenuLevel2
Menu,MyMenu,Add, &Recent, :MenuLevel3
Menu,MyMenu,Add, &Preferences, :MenuLevel4
Menu,MyMenu,Add, &Help, :MenuLevel5
Menu, MyMenu, Color, White
if !n1
	{
	Menu, MenuLevel2, Disable, &Load profile
	Menu, MenuLevel2, Disable, &Edit profile
	Menu, MenuLevel2, Disable, &Delete profile
	}
if AOT
	Menu, MenuLevel4, Check, &Always on top
if fullset
	Menu, MenuLevel4, Check, &Full cycle set
if pastfuture
	Menu, MenuLevel4, Check, &Past && future
;========================================================
;	MAIN GUI
;========================================================
gosub recent
Gui, 1:Color, White, White
Gui, % "1:" (AOT ? "+" : "-") "AlwaysOnTop"
Gui, Add, Text, x5 y5 w600 h55 +Center Hidden,
Gui, Add, Text, x5 y5 w50 h21 +0x200 +Right, Name :
Gui, Add, Text, x5 y27 w50 h20 +0x200 +Right, Born :
Gui, Add, ComboBox, x55 y5 w220 h70 R15 vname gsetdate Choose%lastN%, %profiles%
Gui, Add, DateTime, x55 y27 w220 h20 vDT Choose%lastDate%, dddd`, dd MMMM yyyy
Gui, Add, Button, x5 y50 w30 h25 ghelp, ?
Gui, Add, Button, x55 y50 w60 h25, Create
Gui, Add, Button, x135 y50 w60 h25, Load
Gui, Add, Button, x215 y50 w60 h25 ghideOp, Cancel
; Generated using SmartGuiXP Creator mod 4.3.29.2
if r := CreateChart(rmlib, 1, "3|65|600|370", 1)
	{
	MsgBox, 0x43010, %appname%, CreateChart() returned error %r%`nApplication will now exit.
	ExitApp
	}
Gui, 1:Show, Center w610 h440, %appname%
ControlGet, hRMCtrl, Hwnd,, RMC1, %appname%
OnExit, getout
gosub greeting
gosub calculate
OnMessage(0x200, "move")		; WM_MOUSEMOVE
Return
;========================================================
;	EXIT ROUTINES
;========================================================
reload:
Run, %A_ScriptFullPath%,, UseErrorLevel
ExitApp

GuiClose:
ExitApp

getout:
IniWrite, %AOT%, %inifile%, Generic, AlwaysOnTop
IniWrite, %fullset%, %inifile%, Generic, FullSet
IniWrite, %pastfuture%, %inifile%, Generic, PastAndFuture
IniWrite, %DT%, %inifile%, Generic, LastDate
IniWrite, %lastN%, %inifile%, Generic, LastName
Gdip_Shutdown(pToken)
DllCall("DestroyCursor", Ptr, hCursH)
ExitApp
;========================================================
;	FORMAT LABELS & TIME SPAN
;========================================================
makespan:
i=
i += -Floor(span/2)*pastfuture, days
FormatTime, pMin, %i%, dd MMMM yyyy
labelsH=
j := i
Loop, %span%
	{
	FormatTime, L, %j%, dd.MM
	labelsH .= (j=A_Now ? "TODAY" : L) "*"
	j += 1, days
	}
StringTrimRight, labelsH, labelsH, 1
i += span-1, days
FormatTime, pMax, %i%, dd MMMM yyyy
return
;========================================================
;	CALCULATE
;========================================================
calculate:
Gui, 1:Submit, NoHide
FormatTime, date, %DT%, dddd`, dd MMMM yyyy
DOB := SubStr(DT, 1, 8)
TD := SubStr(A_Now, 1, 8)
EnvSub, TD, %DOB%, d
days := TD - Floor(span/2)*pastfuture
cap := 8*span
i=
i += -Floor(span/2)*pastfuture, days
crit1 := crit2 := ""
Loop, 7
	VarSetCapacity(val%A_Index%, cap, 0)
Loop, %span%
	{
	idx := A_Index-1
	cr=0
	Loop, 7
		{
		j := Sin(2*PI*(days+idx)/cycle%A_Index%)
		k := Sin(2*PI*(days+idx+1)/cycle%A_Index%)
;j := Sin(Mod(days+idx, cycle%A_Index%)*2*PI/cycle%A_Index%)
;k := Sin(Mod(days+idx+1, cycle%A_Index%)*2*PI/cycle%A_Index%)
		NumPut(j, val%A_Index%, 8*idx, "Double")
		if (!Round(j, 1) OR !Round(j+k, 1))
			cr++
		}
	if cr
		{
		j := i
		j += idx, days
		FormatTime, L, %j%, dd.MM
		if cr=1
			crit1 .= L ", "
		else
			crit2 .= L ", "
		}
	}
StringTrimRight, crit1, crit1, 2
StringTrimRight, crit2, crit2, 2
if !crit1
	crit1=none
if !crit2
	crit2=none
if !built
	built := BuildChart(rmlib, 1, &val1, &val2, &val3, &val4, &val5, &val6, &val7)
else UpdateChart(rmlib, 1, &val1, &val2, &val3, &val4, &val5, &val6, &val7)
if fullset
	HideSeries(rmlib, 1, 0, 0, 0, 0, 0, 0, 0)
else HideSeries(rmlib, 1, 0, 0, 0, 1, 1, 1, 1)
gosub makespan
UpdateChartLabels(rmlib, 1, labelsH)
gosub greeting
;GuiControl, 1:, Static1, %title% born %date% (%TD% days old)`nperiod: %pMin% - %pMax%`ncritical days: %crit1%`nmulti-critical days: %crit2%
GuiControl, 1:, Static1,
	(LTrim
	%title% born %date% (%TD% days old)
	period: %pMin% - %pMax%
	critical days: %crit1%
	multi-critical days: %crit2%
	)
gosub hideOp
DrawChart(rmlib, 1)
idx=0
Loop
	{
	IniRead, n%A_Index%, %inifile%, %A_Index%, Name, %A_Space%
	if (idx=prCount)
		break
	if !n%A_Index%
		continue
	if (n%A_Index%=name)
		lastN := A_Index
	idx++
	}
IniWrite, %DT%, %inifile%, Generic, LastDate
IniWrite, %lastN%, %inifile%, Generic, LastName
gosub recent
return
;========================================================
;	GREETING
;========================================================
greeting:
StringMid, bd, DT, 5, 4
StringMid, now, A_Now, 5, 4
title := (bd = now) ? "HAPPY BIRTHDAY " name "," : "Biorhythmic chart for " name
return
;========================================================
;	CHOICE-DISPLAY SWITCH
;========================================================
showOp:
GuiControl, 1:Hide, Static1
DllCall("ShowWindow", Ptr, hRMCtrl, "UInt", FALSE)
Gui, 1:Show, w280 h80
GuiControl, 1:Show, Static2
GuiControl, 1:Show, Static3
GuiControl, 1:Show, ComboBox1
GuiControl, 1:Show, SysDateTimePick321
GuiControl, 1:Show, Button1
GuiControl, 1:Show, Button2
GuiControl, 1:Show, Button3
GuiControl, 1:Show, Button4
return

hideOp:
GuiControl, 1:Hide, Static2
GuiControl, 1:Hide, Static3
GuiControl, 1:Hide, ComboBox1
GuiControl, 1:Hide, SysDateTimePick321
GuiControl, 1:Hide, Button1
GuiControl, 1:Hide, Button2
GuiControl, 1:Hide, Button3
GuiControl, 1:Hide, Button4
Gui, 1:Show, w610 h440
GuiControl, 1:Show, Static1
DllCall("ShowWindow", Ptr, hRMCtrl, "UInt", TRUE)
return
;========================================================
setdate:
Gui, 1:Submit, NoHide
idx=0
Loop
	{
	if (idx=prCount)
		break
	if !n%A_Index%
		continue
	if (n%A_Index%=name)
		{
		GuiControl, 1:, DT, % d%A_Index%
		cProf := A_Index
		}
	idx++
	}
return
;========================================================
;	FILE MENU
;========================================================
new:
GuiControl, 1:, Button2, Create
GuiControl, 1:+gsave, Button2
gosub showOp
GuiControl, 1:+gcalculate, Button3
GuiControl, 1:Show, Button3
return

load:
GuiControl, 1:, Button2, Load
GuiControl, 1:+gcalculate, Button2
gosub showOp
GuiControl, 1:Hide, Button3
return

edit:
GuiControl, 1:, Button2, Save
GuiControl, 1:+gsaveedit, Button2
gosub showOp
GuiControl, 1:Hide, Button3
return

delete:
GuiControl, 1:, Button2, Delete
GuiControl, 1:+gdel, Button2
gosub showOp
GuiControl, 1:Hide, Button3
return

del:
Gui, 1:Submit, NoHide
profiles=
idx=0
Loop
	{
	if (idx=prCount-1 && !n%A_Index%)
		break
	IniRead, n%A_Index%, %inifile%, %A_Index%, Name, %A_Space%
	IniRead, d%A_Index%, %inifile%, %A_Index%, Date, %A_Space%
	if !n%A_Index%
		continue
	if (n%A_Index% = name && d%A_Index% = DT)
		{
		IniDelete, %inifile%, %A_Index%
		Menu, MenuLevel3, Delete, %name%
		j := A_Index
		Loop, %recentcount%
			{
			IniRead, i, %inifile%, Recent, %A_Index%, %A_Space%
			if (i=j)
				IniWrite, %A_Space%, %inifile%, Recent, %A_Index%
			}
		n%A_Index% =
		}
	else
		{
		profiles .= "|" n%A_Index%
		idx++
		}
	}
prCount := idx
IniWrite, %prCount%, %inifile%, Generic, ProfileCount
GuiControl, 1:, name, %profiles%
lastN=1
GuiControl, 1:Choose, name, ||%lastN%
GuiControl, 1:, DT, % d%lastN%
Gui, 1:Submit, NoHide
IniWrite, % d%lastN%, %inifile%, Generic, LastDate
IniWrite, %lastN%, %inifile%, Generic, LastName
IniWrite, %lastN%, %inifile%, Recent, 1
if !profiles
	{
	Menu, MenuLevel2, Disable, &Load profile
	Menu, MenuLevel2, Disable, &Edit profile
	Menu, MenuLevel2, Disable, &Delete profile
	}
goto recent
return

saveedit:
Gui, 1:Submit, NoHide
n%cProf% := name
d%cProf% := DT
IniWrite, %name%, %inifile%, %cProf%, Name
IniWrite, %DT%, %inifile%, %cProf%, Date
profiles=
idx=0
Loop
	{
	if (idx=prCount)
		break
	IniRead, n%A_Index%, %inifile%, %A_Index%, Name, %A_Space%
	if !n%A_Index%
		continue
	else profiles .= "|" n%A_Index%
	idx++
	}
GuiControl, 1:, name, %profiles%
return

save:
Gui, 1:Submit, NoHide
profiles=
idx=0
Loop
	{
	i := A_Index
	if (idx=prCount+1)
		break
	IniRead, n%A_Index%, %inifile%, %A_Index%, Name, %A_Space%
	IniRead, d%A_Index%, %inifile%, %A_Index%, Date, %A_Space%
	if !n%A_Index%
		{
		IniWrite, %name%, %inifile%, %A_Index%, Name
		IniWrite, %DT%, %inifile%, %A_Index%, Date
		profiles .= "|" name "||"
		}
	else profiles .= "|" n%A_Index%
	idx++
	}
prCount := idx
GuiControl, 1:, name, %profiles%
IniWrite, %DT%, %inifile%, Generic, LastDate
IniWrite, %i%, %inifile%, Generic, LastName
IniWrite, %prCount%, %inifile%, Generic, ProfileCount
if profiles
	{
	Menu, MenuLevel2, Enable, &Load profile
	Menu, MenuLevel2, Enable, &Edit profile
	Menu, MenuLevel2, Enable, &Delete profile
	}
return
;========================================================
;	RECENT MENU
;========================================================
recent:
Rpos=
Loop, %recentcount%
	{
	IniRead, i, %inifile%, Recent, %A_Index%, %A_Space%
	if (i=lastN)
		{
		Rpos := A_Index
		break
		}
	}
Rpos := Rpos ? Rpos : recentcount
Gui, 1:Menu
Menu, MenuLevel3, DeleteAll
Loop, % Rpos-1
	{
	IniRead, i, %inifile%, Recent, % Rpos-A_Index, %A_Space%
	IniWrite, %i%, %inifile%, Recent, % Rpos-A_Index+1
	}
IniWrite, %lastN%, %inifile%, Recent, 1
Menu, MenuLevel3, Add, &Clear list, clearrecent
Menu, MenuLevel3, Add
Loop, %recentcount%
	{
	IniRead, i, %inifile%, Recent, %A_Index%, %A_Space%
	if (!i OR !n%i%)
		continue
	Menu, MenuLevel3, Add, % n%i%, loadrecent
	}
Gui, 1:Menu, MyMenu
return

loadrecent:
; separators are counted as items too, so we must subtract both 'Clear' and separator
Loop, %recentcount%
	{
	IniRead, i, %inifile%, Recent, % A_ThisMenuItemPos-3+A_Index, %A_Space%
	if i
		break
	}
if !i
	return
GuiControl, 1:ChooseString, name, % "||" n%i%
GuiControl, 1:, DT, % d%i%
goto calculate
return

clearrecent:
Gui, 1:Menu
Menu, MenuLevel3, DeleteAll
Menu, MenuLevel3, Add, &Clear list, clearrecent
Menu, MenuLevel3, Add
Gui, 1:Menu, MyMenu
Loop, %recentcount%
	IniWrite, %A_Space%, %inifile%, Recent, %A_Index%
return
;========================================================
;	PREFERENCES MENU
;========================================================
AOT:
AOT := !AOT
Gui, % "1:" (AOT ? "+" : "-") "AlwaysOnTop"
Menu, Tray, % (AOT ? "Check" : "Uncheck"), Always on top
Menu, MenuLevel4, % (AOT ? "Check" : "Uncheck"), &Always on top
return
;========================================================
fullset:
fullset := !fullset
Menu, MenuLevel4, % (fullset ? "Check" : "Uncheck"), &Full cycle set
goto calculate
return
;========================================================
pastfuture:
pastfuture := !pastfuture
Menu, MenuLevel4, % (pastfuture ? "Check" : "Uncheck"), &Past && future
goto calculate
return
;========================================================
options:
return
;========================================================
;	HELP MENU
;========================================================
help:
IfNotExist, res\%appname%_help.txt
	FileInstall, res\Biorhythm_help.txt, res\%appname%_help.txt
Run, res\%appname%_help.txt,, UseErrorLevel
return

helpRMC:
Run, res\RMChart.chm,, UseErrorLevel
return

licence:
IfNotExist, res\RMChart_license.txt
	FileInstall, res\RMChart_license.txt, res\RMChart_license.txt
Run, res\RMChart_license.txt,, UseErrorLevel
return
;========================================================
;	'ABOUT' BOX
;========================================================
about:
Font := 0xDD00FF
i := A_DetectHiddenWindows
DetectHiddenWindows, Off
j := WinExist(appname)
Gui 1:+Disabled
if j
	Gui, 1:Hide
Gui, 2:+Owner1 -MinimizeBox +LastFound
Gui, 2:Color, White, White
Gui, 2:Font, c%Font% s8, Tahoma
Gui, 2:Add, Picture, % "x69 y10 w32 h-1" (A_IsCompiled ? " Icon1" : "")
					, % (A_IsCompiled ? A_ScriptName : iconlocal)
Gui, 2:Font, Bold
Gui, 2:Add, Text, x5 y+10 w160 Center, %appname% v%version%
Gui, 2:Font
Gui, 2:Font, c%Font% s8, Tahoma
Gui, 2:Add, Text, xp y+2 wp Center, by Drugwash
Gui, 2:Add, Text, xp y+2 wp Center cBlue hwndhAbt1 gmail, %mylink%
Gui, 2:Add, Text, xp y+10 wp Center, Released %releaseD%
Gui, 2:Add, Text, xp y+2 wp Center, (%releaseT% version)
Gui, 2:Add, Text, xp y+10 wp Center, This product is open-source,
Gui, 2:Add, Text, xp y+2 wp Center, developed in AutoHotkey
Gui, 2:Font,CBlue Underline
Gui, 2:Add, Text, xp y+2 wp Center hwndhAbt2 gahklink, %ahklink%
Gui, 2:Font
Gui, 2:Font, c%Font% s8, Tahoma
abtList := hAbt1 "," hAbt2
if iconpack
	{
	Gui, 2:Add, Text, xp y+5 wp Center, Icons from %iconpack% at
	Gui, 2:Font,CBlue Underline
	Gui, 2:Add, Text, xp y+2 wp Center hwndhAbt3 giconlink, %iconlink2%
	Gui, 2:Font
	Gui, 2:Font, c%Font% s8, Tahoma
	abtList .= "," hAbt3
	}
Gui, 2:Add, Text, xp y+10 wp Center, Uses RMChart library 4.12
Gui, 2:Add, Text, xp y+2 wp Center, built by Rainer Morgen
Gui, 2:Add, Text, xp y+5 wp Center, Thanks Sorana for testing!
Gui, 2:Add, Button, xp+40 y+20 w80 gdismiss Default, &OK
Gui, 2:Show,, %abtwin%
hAbt := WinExist(abtwin)
return
;__________________________________________________________________________
dismiss:
2GuiClose:
Gui, 1:-Disabled
Gui, 2:Destroy
DetectHiddenWindows, %i%
if j
	Gui, 1:Show
Gui, 1:+LastFound
return
;__________________________________________________________________________
mail:
mail = mailto:%mylink%
ahklink:
iconlink:
Run, %A_ThisLabel%,, UseErrorLevel
return
;========================================================
;	FUNCTIONS
;========================================================
CreateChart(lib, g="1", p="5|5|32|32", ctrl="1111")
{
Global Ptr
if !(hGUI := GetHwnd(g))
	{
	msgbox, Error in %A_ThisFunc%():`nInvalid GUI count/handle: %g%.
	Return -100
	}
StringSplit, c, p, |
return DllCall(lib "\RMC_CREATECHART"
			, Ptr, hGUI			; parent hwnd
			, Ptr, ctrl				; control ID
			, "Int", c1				; X
			, "Int", c2				; Y
			, "UInt", c3			; W
			, "UInt", c4			; H
			, "UInt", 0xFF6600CC	; back color
			, "UInt", 3			; control style (RMC_CTRLSTYLE3D)
			, "UInt", 0			; export only
			, "Str", ""				; back image
			, "Str", "Tahoma"		; font name
			, "UInt", 200			; tooltip width
			, "UInt", 0xFFFF55C0)	; bitmap back color
}
;========================================================
DrawChart(lib, ctrl="1111")
{
DllCall(lib "\RMC_DRAW", "UInt", ctrl)
}
;========================================================
BuildChart(lib, ctrl="1111", line1=0, line2=0, line3=0, line4=0, line5=0, line6=0, line7=0)
{
Global AStr, Ptr, PtrP, span, colors, labelsH, legend
r := DllCall(lib "\RMC_ADDREGION"
		, "UInt", ctrl			; control ID
		, "Int", 5				; X
		, "Int", 5				; Y
		, "UInt", 0			; W
		, "UInt", 0			; H
		, "Str", ""				; Footer text
		, "UInt", FALSE)		; Border TRUE

r := DllCall(lib "\RMC_ADDGRID"
		, "UInt", ctrl			; control ID
		, "UInt", 1			; region
		, "UInt", 0xFF000000	; back color
		, "UInt", FALSE		; gradient
		, "Int", 0				; X
		, "Int", 0				; Y
		, "UInt", 0			; W
		, "UInt", 0			; H
		, "UInt", 0)			; bicolor (RMC_BICOLOR_NONE)

labelsV := "*-*0*+*"
r := DllCall(lib "\RMC_ADDDATAAXIS"
		, "UInt", ctrl			; control ID
		, "UInt", 1			; region
		, "UInt", 1			; alignment (RMC_DATAAXISLEFT)
		, "Double", -1			; MinValue
		, "Double", 1			; MaxValue
		, "UInt", 5			; tick count (horizontal lines)
		, "UInt", 7			; font size
		, "UInt", 0xFFF0E68C	; text color Khaki
		, "UInt", 0xFFD8BFD8	; line color Thistle
		, "UInt", 2			; line style (RMC_LINESTYLEDOT)
		, "UInt", 1			; decimal digits
		, "Str", ""				; unit
		, AStr, "Cycle Score"	; text
		, AStr, labelsV			; labels
		, "UInt", 0)			; label alignment (RMC_TEXTCENTER)

r := DllCall(lib "\RMC_ADDLABELAXIS"
		, "UInt", ctrl			; control ID
		, "UInt", 1			; region
		, AStr, labelsH			; labels
		, "UInt", 1			; axis count
		, "UInt", span			; tick count (days to show)
		, "UInt", 8			; alignment (RMC_LABELAXISBOTTOM)
		, "UInt", 7			; font size
		, "UInt", 0xFFF0E68C	; text color Khaki
		, "UInt", 4			; text alignment (RMC_TEXTUPWARD)
		, "UInt", 0xFF696969	; line color DimGray
		, "UInt", 2			; line style (RMC_LINESTYLEDOT)
		, AStr, "Date (as Day.Month)")			; text

r := DllCall(lib "\RMC_ADDLEGEND"
		, "UInt", ctrl			; control ID
		, "UInt", 1			; region
		, AStr, legend			; legend text
		, "UInt", 1			; alignment (RMC_LEGEND_TOP)
		, "UInt", 0xFF009933	; back color GrassGreen
		, "UInt", 1			; style (RMC_LEGENDNORECT)
		, "UInt", 0xFFF0E68C	; text color Khaki
		, "UInt", 7			; font size
		,"UInt", FALSE)		; font bold

Loop, Parse, colors, CSV
	if r := DllCall(lib "\RMC_ADDLINESERIES"
		, "UInt", ctrl			; control ID
		, "UInt", 1			; region
		, Ptr, line%A_Index%	; array
		, "UInt", span			; data values count
		, "UInt", 0			; first PPC value
		, "UInt", 0			; PPC values count
		, "UInt", 21			; type (RMC_LINE)
		, "UInt", 21			; style (RMC_LINE_FLAT)
		, "UInt", 2			; line style (RMC_LSTYLE_SPLINE)
		, "UInt", FALSE		; is lucent
		, "UInt", A_LoopField	; color (Cyan)
		, "UInt", 0			; symbol (RMC_SYMBOL_NONE)
		, "UInt", 1			; which data axis
		, "UInt", 0			; value label on (RMC_VLABEL_NONE)
		, "UInt", 0)			; hatch mode (RMC_HATCHBRUSH_OFF)
	msgbox, Error %r% in RMC_ADDLINESERIES()
return TRUE
}
;========================================================
UpdateChart(lib, ctrl="1111", line1=0, line2=0, line3=0, line4=0, line5=0, line6=0, line7=0)
{
Global Ptr, span, colors
Loop, Parse, colors, CSV
if r := DllCall(lib "\RMC_SETSERIESDATA"
		, "UInt", ctrl			; control ID
		, "UInt", 1			; region
		, "UInt", A_Index		; series
		, Ptr, line%A_Index%	; first data value (DOUBLE)
		, "UInt", span			; data values count
		, "UInt", FALSE)		; Y data
	msgbox, Error %r% in RMC_SETSERIESDATA()
}
;========================================================
UpdateChartLabels(lib, ctrl="1111", labels="")
{
Global AStr, labelsH
DllCall(lib "\RMC_SETLAXLABELS"
		, "UInt", ctrl			; control ID
		, "UInt", 1			; region
		, AStr, labelsH)			; labels
}
;========================================================
HideSeries(lib, ctrl="1111", h1=0, h2=0, h3=0, h4=0, h5=0, h6=0, h7=0)
{
Global colors
Loop, Parse, colors, CSV
if r := DllCall(lib "\RMC_SETSERIESHIDE"
		, "UInt", ctrl			; control ID
		, "UInt", 1			; region
		, "UInt", A_Index		; series
		, "UInt", h%A_Index%)	; series
	msgbox, Error %r% in RMC_SETSERIESHIDE()
}
;========================================================
GetSysPath(ByRef sp)
{
Global AW, Ptr
sz := 256*2**(A_IsUnicode=TRUE)
VarSetCapacity(sp, sz, 0)
DllCall("SHFOLDER\SHGetFolderPath" AW, "UInt", 0, "UInt", 0x0025, "UInt", 0, "UInt", 0, Ptr, &sp)	; CSIDL_SYSTEM, SHGFP_TYPE_CURRENT
}
;========================================================
Gdip_Startup()
{
Global PtrP, Ptr
If !DllCall("GetModuleHandle", "Str", "gdiplus")
	DllCall("LoadLibrary", "Str", "gdiplus")
VarSetCapacity(si, 16, 0), si := Chr(1)
DllCall("gdiplus\GdiplusStartup", PtrP, pToken, Ptr, &si, "UInt", 0)
Return pToken
}

Gdip_Shutdown(pToken)
{
Global Ptr
DllCall("gdiplus\GdiplusShutdown", "UInt", pToken)
If hModule := DllCall("GetModuleHandle", "Str", "gdiplus")
	DllCall("FreeLibrary", Ptr, hModule)
Return 0
}
;========================================================
;	WM_MOUSEMOVE HOOK
;========================================================
move(wP, lP)
{
Global
Local x, winID, ctrl
Critical
MouseGetPos, x,, winID, ctrl, 2
if (winID=hAbt)
	if ctrl in %abtList%
		DllCall("SetCursor", "UInt", hCursH)
if (winID=hSet)
	if ctrl in %setList%
		DllCall("SetCursor", "UInt", hCursH)
if (winID=hMain)
	{
	if ctrl in %imgHlist%
		DllCall("SetCursor", "UInt", hCursH)
	}
}

;========================================================
#include lib\func_GetHwnd.ahk
